// const {createApp, ref} = Vue 
    
// createApp({
//     setup() {
//         const name = ref("Muhammad Al-amin Abubakar")
//         return{
//             name,
//             first_number:0,
//             second_number:0,
//             result:0
//         }
//     }        
// }).mount("#app")

const app1 = Vue.createApp({
    data(){
        return{
            name: "Muhammad Al-amin Abubakar",
            first_number:0,
            second_number:0,
            result:0,
            qty:0,
            balance:10
        }
    },

    methods:{
        addition(){
            console.log("First Number: " + this.first_number + " " + "Second Number: " + this.second_number);
            this.result = Number(this.first_number) + Number(this.second_number);
        },
        subtraction(){
            console.log("First Number: " + this.first_number + " " + "Second number: " + this.second_number);
            this.result = Number(this.first_number) - Number(this.second_number);
        },
        multiplication(){
            console.log("First number: " + this.first_number + " " + "Second Number: " + this.second_number);
            this.result = Number(this.first_number) * Number(this.second_number);
        },
        division(){
            console.log("First Number: " + this.first_number + " " + "Second Number: " + this.second_number);
            this.result = Number(this.first_number) / Number(this.second_number);
        },
        firstInput(num){
            this.first_number = num;
        },
        getBal(){
            console.log("Quantity: " + this.qty);
            this.balance -= Number(this.qty); 
        }
    }
})
app1.mount("#app")